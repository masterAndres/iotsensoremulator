# Iot Emulate

## Descripción de Iot Emulate

Realize unas pequeñas modificaciones a este archivo para poder integrarlo de una manera más facil a mi desarrollo. La diferencia más notable es que agregué la clase sensor, que es la que se encarga de generar los sensores y sus valores. Traslde a este clase todas las funciones que tenian para generar las lecturas.

Notaran que el socket.io tiene comentado el uso de un token. Esto es por que por lado del dashboard no pude implementar adecuadamente le token en socket por lo que lo deshabilete para ambos canales (app y sensores), originalmente sin el token no era posible inyectar valores en la api.

## Requerimientos

* Ejecutar el comando  `npm install`.

## Comandos

* Para iniciar el servicio `node .\app.js`.

# Estructura  de la WeApp.

* __app.js__- El archivo principal que se encarga de inyectar las peticiones al servidor.
* __sensor.js__- Clase para generar los objetos sensores.
* __.gitignore__- Aquí colocamos el listado de carpetas y archivos que no deben estar en el repo.
* __config.js__- Aquí colocamos las variables que se modifican en cada servidor, como la DB y puertos utilizados.
* __package.json__- Metadatos usuales, relevantes para el proyecto.
* __README.md__- Este documento.
