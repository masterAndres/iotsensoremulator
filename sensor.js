const TEMP_LIMITS = [10, 25];
const HUM_LIMITS = [30, 70];

module.exports = class Sensor {
    constructor(id, sensor, value) {
        this.id = id;
        this.sensor = sensor;
        this.value = value;
    }

    generateData() {
        var sig = Math.random() > .5 ? 1 : -1,
            value = sig * parseFloat(Math.random().toFixed(1)),
            LIMITS = null;
        if (this.sensor == "TEMP") LIMITS = TEMP_LIMITS;
        if (this.sensor == "HUM") LIMITS = HUM_LIMITS;
        if (this.value + value >= LIMITS[0] && this.value + value <= this.value[1]) this.value += value;
        else this.value -= value;
    }

    getData() {
        return {
            sensorId: this.id,
            sensor: this.sensor,
            value: this.value
        }
    }
}