var config = require('./config')
var socket = require('socket.io-client');
const HOSTNAME = config.hostname;
const PORT = config.port;
const TOPIC = config.topic;

const SENSOR = require('./sensor');

var hum = new SENSOR("e200a43b-7566-41cd-8592-a924bc289f38","HUM",50);
var temp = new SENSOR("70b55e7d-640c-4c81-9b69-ad1d561f4661","TEMP",17.5);



socket = socket('http://' + HOSTNAME + ':' + PORT/* ,{
    query:{
        token:config.token
    }
} */);
socket.on('connect_error', (error) => {
    console.log("socket error", error);
});

socket.on('connect', function() {
    console.log("socket connected", socket.id);
    setInterval(() => {
        getDataAndSend();
    }, 10000);
});

function getDataAndSend() {
    hum.generateData();
    temp.generateData();
    socket.emit(TOPIC, hum.getData());
    socket.emit(TOPIC, temp.getData());
}


